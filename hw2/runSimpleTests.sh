#!/bin/bash

#Case1
start=$(date +%s.%N)
outputMain=$(./primes 2 7)
runtime=$(echo "$(date +%s.%N) - $start" | bc)
outputTest=$(./primeSol 2 7)




echo "Case 1: "
if [ "$outputMain" == "$outputTest" ] ; then
    echo -e "Success with runtime $runtime \n"
else
    echo -e "Failure \n"
fi

#Case 2
start=$(date +%s.%N)
outputMain=$(./primes 2 100)
runtime=$(echo "$(date +%s.%N) - $start" | bc)
outputTest=$(./primeSol 2 100)


echo "Case 2: "
if [ "$outputMain" == "$outputTest" ] ; then
    echo -e "Success with runtime $runtime \n"
else
    echo -e "Failure \n"
fi

#Case 3
start=$(date +%s.%N)
outputMain=$(./primes 5 50)
runtime=$(echo "$(date +%s.%N) - $start" | bc)
outputTest=$(./primeSol 5 50)

echo "Case 3: "
if [ "$outputMain" == "$outputTest" ] ; then
    echo -e "Success with runtime $runtime \n"
else
    echo -e "Failure \n"
fi


#Case 4
start=$(date +%s.%N)
outputMain=$(./primes 24 100)
runtime=$(echo "$(date +%s.%N) - $start" | bc)
outputTest=$(./primeSol 24 100)



echo "Case 4: "
if [ "$outputMain" == "$outputTest" ] ; then
    echo -e "Success with runtime $runtime \n"
else
    echo -e "Failure \n"
fi


#Case 5
start=$(date +%s.%N)
outputMain=$(./primes 2 199)
runtime=$(echo "$(date +%s.%N) - $start" | bc)
outputTest=$(./primeSol 2 199)


echo "Case 5: "
if [ "$outputMain" == "$outputTest" ] ; then
    echo -e "Success with runtime $runtime \n"
else
    echo -e "Failure \n"
fi
