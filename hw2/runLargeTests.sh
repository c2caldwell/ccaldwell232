#!/bin/bash
start=$(date +%s.%N)
outputMain=$(./primes 50000 1000000)
runtime=$(echo "$(date +%s.%N) - $start" | bc)
outputTest=$(./primeSol 50000 1000000)
echo "Case 1: "
if [ "$outputMain" == "$outputTest" ] ; then
    echo -e "Success with runtime $runtime \n"
else
    echo -e "Failure \n"
fi
