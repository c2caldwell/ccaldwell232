#include <stdio.h>
#include <stdlib.h>

generatePrimes(int lowerBound, int upperBound){
        printf("All of the following numbers between %d and %d are prime: ", lowerBound, upperBound);
        int x, y;
        for(x = lowerBound+1; x<= upperBound-1; x++){
            for(y=2;y<x; y++)
                if(x%y == 0)
                    break;

            if(y == x)
                printf("%d ", x);
        }


    }

int main ( int argc, char *argv[]){

    if(argc != 3) {
        printf("Only enter two integer arguments, please");
    }
    else{
        int lowerBound = atoi(argv[1]);
        int upperBound = atoi(argv[2]);
        generatePrimes(lowerBound, upperBound);
    }
}